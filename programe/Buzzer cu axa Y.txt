Rte_Read_InJoy_JoySteer(&Status);
   //Buzz with AxisY
   if (Status.t_Press == RTE_JOY_PRESSED)
   {
      Buzz_output.t_Enable = RTE_BUZZ_ON;
      //min period is 100ms
      //max period is 1000ms
      Buzz_output.t_Period = 100 + (9 * Status.t_AxisY) / 100;
   }
   else
   {
      Buzz_output.t_Enable = RTE_BUZZ_OFF;
      Buzz_output.t_Period = 0;

   }
   Rte_Write_OutBuzz_BuzzHorn(&Buzz_output);