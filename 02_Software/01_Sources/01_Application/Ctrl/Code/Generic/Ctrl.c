/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Ctrl.c
 *    \author     Nedelcu Andrei-Cosmin
 *    \brief      The Ctrl (Control) SWC shall implement the system�s states and transitions between the states
 *    \           by making use of the other 3 peripheral drivers.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/
#include "Rte_Ctrl.h"

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                            Definition Of Global Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Definition Of Global Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Variables                                            */
/*-------------------------------------------------------------------------------------------------------------------*/
Rte_JoyStsType Status;
Rte_ServoCtrlType AxisX, AxisY;
Rte_BuzzCtrlType Buzz_output;
uint32 UnghiA, UnghiB;
uint8 ok, contor, caz, movement;
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Constants                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Export Of Global Functions                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/

void Ctrl_Init(void)
{
   caz = 1; //sistemul va intra in Steering dupa un reset; 2- Steering&Horn; 3- Configuration
   movement = 1; //sistemul va incepe in parallel dupa un reset; cazul 2 este pentru Akerman
   contor = 0;
}
//declararea functiilor pentru virajul motoaraselor-----------------------------------
void CONFIG_STEERING_PARALLEL(void)
{
   AxisX = (uint32) (3000 + (Status.t_AxisX * 12) / 10);
   Rte_Write_OutServo_ServoSteerLeft(&AxisX);
   Rte_Write_OutServo_ServoSteerRight(&AxisX);
}
//-------------------------------------------------------------------------------------
void CONFIG_STEERING_AKERMAN(void)
{
   //limited between 3000 and 15000 for [-60;60] angles
   //Ackerman
   AxisX = (uint32) (3000 + (Status.t_AxisX * 12) / 10);
   //cazul de la 9000-15000
   if (AxisX > 9000)
   {
      AxisY = ((AxisX - 9000) * 60) / 6000;
      ok = 1;
   }
   else
   {
      ok = 2;
      AxisY = 9000 - AxisX;
      AxisX = 9000 + AxisY;
      AxisY = ((AxisX - 9000) * 60) / 6000;
   }

   if (AxisY <= 5)
   {
      UnghiB = AxisY;
      UnghiA = ((9654 * AxisY) + 190) / 10000;
   }
   else if (AxisY <= 20)
   {
      UnghiB = AxisY;
      UnghiA = ((8212 * AxisY) + 8360) / 10000;
   }
   else if (AxisX <= 40)
   {
      UnghiB = AxisY;
      UnghiA = ((658 * AxisY) + 4320) / 1000;
   }
   else
   {
      UnghiB = AxisY;
      UnghiA = ((6116 * AxisY) + 61600) / 10000;
   }

   if (ok == 1)
   {
      AxisX = 100 * UnghiA + 9000;
      AxisY = 100 * UnghiB + 9000;
      Rte_Write_OutServo_ServoSteerLeft(&AxisX);
      Rte_Write_OutServo_ServoSteerRight(&AxisY);
   }
   else
   {
      AxisX = 9000 - 100 * UnghiA;
      AxisY = 9000 - 100 * UnghiB;
      Rte_Write_OutServo_ServoSteerLeft(&AxisY);
      Rte_Write_OutServo_ServoSteerRight(&AxisX);
   }
}
//-------------------------------------------------------------------------------
void BUZZER(void)
{
   //Buzz with AxisY
   if (Status.t_Press == RTE_JOY_PRESSED)
   {
      Buzz_output.t_Enable = RTE_BUZZ_ON;
      //min period is 100ms
      //max period is 1000ms
      Buzz_output.t_Period = 100 + (9 * Status.t_AxisY) / 100;
   }
   else
   {
      Buzz_output.t_Enable = RTE_BUZZ_OFF;
      Buzz_output.t_Period = 0;

   }
   Rte_Write_OutBuzz_BuzzHorn(&Buzz_output);
}
//-------------------------------------------------------------------------------
void ENTER_CONFIGURATION(void)
{
   //se citeste butonul
   if (Status.t_Press == RTE_JOY_PRESSED)
   {
      contor++;
   }
   else if (Status.t_Press == RTE_JOY_NOT_PRESSED)
   {
      if (contor >= 10)
      {
         caz = 3; // intra in modul configuration
      }
      contor = 0;
   }
}
//-----------------------------------------------------------------------------
void FALLING_EDGE(void)
{
   //se citeste butonul si se face tranzitia dintre cele 2 stari
   if (Status.t_Press == RTE_JOY_PRESSED)
   {
      contor++;
   }
   else if (Status.t_Press == RTE_JOY_NOT_PRESSED)
   {
      if (contor < 10)
      {
         if (caz == 2) //daca e in cazul STEERING&HORN atunci el va intra in cazul STEERING
         {
            caz = 1;
         }
         else if (caz == 1) //invers
         {
            caz = 2;
         }
         else //daca e in CONFIGURATION atunci va intra in cazul STEERING
         {
            caz = 1;
         }
      }
      contor = 0;
   }
}
//--------------------------------------------------------------
void Ctrl_Main(void)
{
   Rte_Read_InJoy_JoySteer(&Status);
   switch (caz)
   {
      case 1:
      //STEERING
      {
         ENTER_CONFIGURATION();
         FALLING_EDGE();
         if (movement == 1)
         {
            CONFIG_STEERING_PARALLEL();
         }
         else
         {
            CONFIG_STEERING_AKERMAN();
         }
         break;
      }

      case 2:
      //STEERING & HORN
      {
         ENTER_CONFIGURATION();
         FALLING_EDGE();
         if (movement == 1)
         {
            CONFIG_STEERING_PARALLEL();
         }
         else
         {
            CONFIG_STEERING_AKERMAN();
         }
         BUZZER();
         break;
      }
      case 3:
      //CONFIGURATION
      {
         FALLING_EDGE();
         Rte_ServoCtrlType unghi = 2000;
         Rte_Write_OutServo_ServoSteerLeft(&unghi);
         Rte_Write_OutServo_ServoSteerRight(&unghi);
         //mai sus am un set de comenzi care ma ajuta sa imi dau seama vizual daca am intrat in modul configuration
         if (Status.t_AxisY < 4000) //
         {
            movement = 1; //cazul paralel
         }
         else if (Status.t_AxisY > 6000)
         {
            movement = 2; //cazul akermann
         }
         //else nothing fiindca movement va ramane acelasi daca axa y va ramane intre valorile 4000 si 6000
         break;
      }
   }

}
