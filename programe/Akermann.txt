Rte_JoyStsType Status;
Rte_ServoCtrlType AxisX, AxisY;
Rte_BuzzCtrlType Buzz_output;
uint32 UnghiA;
uint32 UnghiB;
uint8 ok;




Rte_Read_InJoy_JoySteer(&Status);
   //limited between 3000 and 15000 for [-60;60] angles
   //Ackerman
   AxisX = (uint32) (3000 + (Status.t_AxisX * 12) / 10);
   //cazul de la 9000-15000
   if (AxisX > 9000)
   {
      AxisY = ((AxisX - 9000) * 60) / 6000;
      ok = 1;
   }
   else
   {
      ok = 2;
      AxisY = 9000 - AxisX;
      AxisX = 9000 + AxisY;
      AxisY = ((AxisX - 9000) * 60) / 6000;
   }

   if (AxisY <= 5)
   {
      UnghiB = AxisY;
      UnghiA = ((9654 * AxisY) + 190) / 10000;
   }
   else if (AxisY <= 20)
   {
      UnghiB = AxisY;
      UnghiA = ((8212 * AxisY) + 8360) / 10000;
   }
   else if (AxisX <= 40)
   {
      UnghiB = AxisY;
      UnghiA = ((658 * AxisY) + 4320) / 1000;
   }
   else
   {
      UnghiB = AxisY;
      UnghiA = ((6116 * AxisY) + 61600) / 10000;
   }

   if (ok == 1)
   {
      AxisX = 100 * UnghiA + 9000;
      AxisY = 100 * UnghiB + 9000;
      Rte_Write_OutServo_ServoSteerLeft(&AxisX);
      Rte_Write_OutServo_ServoSteerRight(&AxisY);
   }
   else
   {
      AxisX = 9000 - 100 * UnghiA;
      AxisY = 9000 - 100 * UnghiB;
      Rte_Write_OutServo_ServoSteerLeft(&AxisY);
      Rte_Write_OutServo_ServoSteerRight(&AxisX);
   }