/*-------------------------------------------------------------------------------------------------------------------*/
/**
 *    \file       Joy.c
 *    \author     Nedelcu Andrei-Cosmin
 *    \brief      Empty template to be used for all .c files. This file provides an example for the case in which a
 *                detailed description is not needed.
 */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                                     Inclusions                                                    */
/*-------------------------------------------------------------------------------------------------------------------*/
#include "Rte_Joy.h"
#include "Dio.h"
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                             Definition Of Local Macros                                            */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Definition Of Local Data Types                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Variables                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

Rte_JoyPressType Joy_Press = RTE_JOY_NOT_PRESSED;
uint8 Joy_Button_Counter;

Adc_ValueGroupType MovAvrg_x[3];
uint8 Index_x = 0;
Adc_ValueGroupType MovAvrg_y[3];
uint8 Index_y = 0;

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                          Declaration Of Global Constants                                          */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                           Declaration Of Local Functions                                          */
/*-------------------------------------------------------------------------------------------------------------------*/
extern void Joy_Main(void);
extern void Joy_Init(void);
/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Local Functions                                         */
/*-------------------------------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------------------------*/
/*                                         Implementation Of Global Functions                                        */
/*-------------------------------------------------------------------------------------------------------------------*/
void Joy_Init(void)
{
   uint8 i;
   Joy_Press = RTE_JOY_NOT_PRESSED;
   Joy_Button_Counter = 0;
   for (i = 0; i < 3; i++)
   {
      MovAvrg_x[i] = 2048;
      MovAvrg_y[i] = 2048;
   }
}
void Joy_Main(void)
{
   uint32 Sum;
   uint8 i;
   Adc_ValueGroupType Joy_AxisAdcX;
   Adc_ValueGroupType Joy_AxisAdcY;
   //Button
   Rte_JoyStsType Status;

   Dio_LevelType Level;
   Rte_JoyPressType Press;

   Level = Dio_ReadChannel(IOHWAB_JOY_STEER_BTN);
   if (STD_LOW == Level)
   {
      Press = RTE_JOY_PRESSED;
   }
   else
   {
      Press = RTE_JOY_NOT_PRESSED;
   }

   if (Press != Joy_Press)
   {
      Joy_Button_Counter++;
      if (Joy_Button_Counter >= 3)
      {
         Joy_Press = Press;
         Joy_Button_Counter = 0;
      }
   }
   else
   {
      Joy_Button_Counter = 0;
   }
   Status.t_Press = Joy_Press;

//For X axis
   Adc_SetupResultBuffer(IOHWAB_JOY_STEER_AXIS_X, &Joy_AxisAdcX);
   Adc_StartGroupConversion(IOHWAB_JOY_STEER_AXIS_X);
   while (ADC_BUSY == Adc_GetGroupStatus(IOHWAB_JOY_STEER_AXIS_X))
   {
      /* Wait for the conversion to finish. */
   }

   MovAvrg_x[Index_x] = Joy_AxisAdcX;

   if (Index_x == 2)
   {
      Index_x = 0;
   }
   else
   {
      Index_x++;
   }

   Sum = 0;
   for (i = 0; i < 3; i++)
   {
      Sum += MovAvrg_x[i];
   }
   Status.t_AxisX = Sum / 3;
   Status.t_AxisX = (Status.t_AxisX * 10000) / 4095;

   //For Y axis
   Adc_SetupResultBuffer(IOHWAB_JOY_STEER_AXIS_Y, &Joy_AxisAdcY);
   Adc_StartGroupConversion(IOHWAB_JOY_STEER_AXIS_Y);
   while (ADC_BUSY == Adc_GetGroupStatus(IOHWAB_JOY_STEER_AXIS_Y))
   {
      /* Wait for the conversion to finish. */
   }

   MovAvrg_y[Index_y] = Joy_AxisAdcY;

   if (Index_y == 2)
   {
      Index_y = 0;
   }
   else
   {
      Index_y++;
   }

   Sum = 0;
   for (i = 0; i < 3; i++)
   {
      Sum += MovAvrg_y[i];
   }
   Status.t_AxisY = Sum / 3;
   Status.t_AxisY = (Status.t_AxisY * 10000) / 4095;

   Rte_Write_Out_JoySteer(&Status);
}
